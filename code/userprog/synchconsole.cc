// synchconsole.cc 


#ifdef CHANGED

#include "copyright.h"
#include "synchconsole.h"
#include "system.h"
#include "synch.h"





static Semaphore *readAvail;
static Semaphore *writeDone;

static Semaphore *protegeEcriture;



static void ReadAvail(int arg){readAvail->V();}
static void WriteDone(int arg){writeDone->V();}





//----------------------------------------------------------------------
// SynchConsole::SynchConsole
// 	(commentaires à mettre à jour)
//----------------------------------------------------------------------

SynchConsole::SynchConsole(char *readFile, char *writeFile)
{
    readAvail = new Semaphore("read avail", 0);
    writeDone = new Semaphore("write done", 0);
    protegeEcriture = new Semaphore("protege ecriture", 1);

   console = new Console(&*readFile, &*writeFile, ReadAvail, WriteDone, 0);
}



//----------------------------------------------------------------------
// SynchConsole::~SynchConsole
// 	Clean up ....
//----------------------------------------------------------------------

SynchConsole::~SynchConsole()
{
    delete console;
    delete writeDone;
    delete readAvail;
}




//----------------------------------------------------------------------
// SynchConsole::SynchGetChar()
// 	
//
//----------------------------------------------------------------------

char
SynchConsole::SynchGetChar()
{
    char ch;
    readAvail->P();
    ch = console->GetChar();
    return ch;
}

//----------------------------------------------------------------------
// SynchConsole::SynchPutChar()
// 	
//
//----------------------------------------------------------------------

void
SynchConsole::SynchPutChar(const char ch)
{
    protegeEcriture->P();
    console->PutChar(ch);
    writeDone->P();
    protegeEcriture->V();
}



//----------------------------------------------------------------------
// SynchConsole::SynchPutString(const char *s)
// 	
//
//----------------------------------------------------------------------

void
SynchConsole::SynchPutString(const char *s)
{
    int compt = 0;
    char ch=*(s+compt);
    
    while (ch != 0){
        SynchPutChar(ch);
        compt++;
        ch = *(s+compt);          
    }


}


#endif
