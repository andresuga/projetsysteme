// exception.cc 
//      Entry point into the Nachos kernel from user programs.
//      There are two kinds of things that can cause control to
//      transfer back to here from user code:
//
//      syscall -- The user code explicitly requests to call a procedure
//      in the Nachos kernel.  Right now, the only function we support is
//      "Halt".
//
//      exceptions -- The user code does something that the CPU can't handle.
//      For instance, accessing memory that doesn't exist, arithmetic errors,
//      etc.  
//
//      Interrupts (which can also cause control to transfer from user
//      code into the Nachos kernel) are handled elsewhere.
//
// For now, this only handles the Halt() system call.
// Everything else core dumps.
//
// Copyright (c) 1992-1993 The Regents of the University of California.
// All rights reserved.  See copyright.h for copyright notice and limitation 
// of liability and disclaimer of warranty provisions.

#include "copyright.h"
#include "system.h"
#include "syscall.h"

//----------------------------------------------------------------------
// UpdatePC : Increments the Program Counter register in order to resume
// the user program immediately after the "syscall" instruction.
//----------------------------------------------------------------------
static void
UpdatePC ()
{
    int pc = machine->ReadRegister (PCReg);
    machine->WriteRegister (PrevPCReg, pc);
    pc = machine->ReadRegister (NextPCReg);
    machine->WriteRegister (PCReg, pc);
    pc += 4;
    machine->WriteRegister (NextPCReg, pc);
}


//----------------------------------------------------------------------
// ExceptionHandler
//      Entry point into the Nachos kernel.  Called when a user program
//      is executing, and either does a syscall, or generates an addressing
//      or arithmetic exception.
//
//      For system calls, the following is the calling convention:
//
//      system call code -- r2
//              arg1 -- r4
//              arg2 -- r5
//              arg3 -- r6
//              arg4 -- r7
//
//      The result of the system call, if any, must be put back into r2. 
//
// And don't forget to increment the pc before returning. (Or else you'll
// loop making the same system call forever!
//
//      "which" is the kind of exception.  The list of possible exceptions 
//      are in machine.h.
//----------------------------------------------------------------------



void StartUserThread(int nb){

    printf("[StartUserThread] debut,   verification argument : valeur : %d , %x   *************]\n\n",nb, nb);
    currentThread->Print();
    
    // nb est l'adresse dans le tas
    int *ptr =(int *) nb;


    int valeur = *ptr;
    int valeur2 = *(ptr+1);
    int stacktop = *(ptr+2);
    printf("[StartUserThread] suite,   verification adresse :  pointeur : %p  -   valeur  :  %d et %d et %d *************] \n\n",ptr,  valeur,valeur2, stacktop );

    currentThread->space->InitRegisters();
    currentThread->space->RestoreState();
    machine->WriteRegister(PCReg, valeur);
    machine->WriteRegister(NextPCReg, valeur+4);
    machine->WriteRegister(StackReg, stacktop-200);
    machine->Run();

    printf("[StartUserThread] fin  ............................... \n\n");

}










void
ExceptionHandler (ExceptionType which)
{
    int type = machine->ReadRegister (2);

#ifndef CHANGED 

    if ((which == SyscallException) && (type == SC_Halt))
      {
	  DEBUG ('a', "Shutdown, initiated by user program.\n");
	  interrupt->Halt ();
      }
    else
      {
	  printf ("Unexpected user mode exception %d %d\n", which, type);
	  ASSERT (FALSE);
      }

#else // CHANGED

    if (which == SyscallException) {
      switch (type) {

      case SC_Halt: {
        DEBUG ('a', "Shutdown, initiated by user program.\n");
        interrupt->Halt ();
        break;
      }



      case SC_Exit: {
        DEBUG ('a', "Appel systeme ******** EXIT ********* \nThread courant : %s \n\n", currentThread->getName() );

        while(currentThread->nbFils>0){
             printf("Nombre de FILS : %d \n\n", currentThread->nbFils);          
             printf("en attente \n");
             currentThread->Yield();
        }
        interrupt->Halt();

        break;
      }

   

      case SC_PutChar: {
        DEBUG ('a', "PutChar, écriture, initiated by user program.\n");
        int car = machine->ReadRegister (4);
        // printf("which : %d  -  type : %d  -  car : %d \n\n", which, type, car);
        synchconsole->SynchPutChar (car);

        break;
      }


      case SC_PutString: {
        DEBUG ('a', "PutString, écriture, initiated by user program.\n");
        int adresse = machine->ReadRegister (4);

        int res;
        machine->ReadMem(adresse, 1, &res);

        int k=0;       
 
        while(res!=0){
            synchconsole->SynchPutChar (res);
            k++;
            machine->ReadMem(adresse+k, 1, &res);
        }
        
        break;
      }


      case SC_GetChar: {
        DEBUG ('a', "GetChar, lecture, initiated by user program.\n");
        int car = synchconsole->SynchGetChar ();
        machine->WriteRegister (2, car);

        break;
      }



      case SC_UserThreadCreate: {
        DEBUG ('a', "UserThreadCreate......DEBUG a ...\n");
        DEBUG ('t', "UserThreadCreate......DEBUG t ...\n");
        int arg1 = machine->ReadRegister(4);
        int arg2 = machine->ReadRegister(5);

        int stacktop = machine->ReadRegister(StackReg); //29

        printf("[Appel Systeme : UserThreadCreate] \n which : %d  -  type : %d  \narg1 : %d  -   arg2 : %d  \nstacktop : %d  \n\n", which, type, arg1, arg2,  stacktop); 


        // inscription des données dans le tas pour transmission via l'appel à la fonction StartUserThread
        //  inscription possible dans la pile du nouveau thread ? non car la pile du nouveau thread n'est pas encore allouée
        int *adresse;
        adresse = new int[3];
        *adresse = arg1;
        *(adresse+1) = arg2;
        *(adresse+2) = stacktop;
        //printf("adresse : %p    -   valeur : %d \n\n", adresse, *adresse);


        Thread *t = new Thread("threadSC");
        t->Fork(StartUserThread,(int) adresse);
//currentThread->Yield();

        t->pere = currentThread;
        currentThread->nbFils += 1;

        printf("[case SC_UserThreadCreate] après le fork  \n\n"); 


        break;
      }



      case SC_UserThreadExit: {
        DEBUG ('a', "UserThreadExit, destruction thread, initiated by user program.\n");
        
        currentThread->pere->nbFils -= 1;

        currentThread->Finish();

        break;
      }

      

      default: {
	  printf ("Unexpected user mode exception %d %d\n", which, type);
	  ASSERT (FALSE);
      }




  }






    // LB: Do not forget to increment the pc before returning!
    UpdatePC ();
    // End of addition

  }
}


#endif // CHANGED
