// synchconsole.h 



#ifdef CHANGED

#ifndef SYNCHCONSOLE_H
#define SYNCHCONSOLE_H

#include "copyright.h"
#include "utility.h"
#include "console.h"


class SynchConsole {
  public:
    SynchConsole(char *readFile, char *writeFile);
				// initialize the hardware console device
    ~SynchConsole();			// clean up console emulation

   

// external interface
    void SynchPutChar(const char ch);	

    char SynchGetChar();	

    void SynchPutString(const char *s);

    void SynchGetString(char *s, int n);

 

  private:

    Console *console;

 //   static void WriteDone(int arg);

//    static void ReadAvail(int arg);

};

#endif // SYNCHCONSOLE_H

#endif
