/* halt.c
 *	Simple program to test whether running a user program works.
 *	
 *	Just do a "syscall" that shuts down the OS.
 *
 * 	NOTE: for some reason, user programs with global data structures 
 *	sometimes haven't worked in the Nachos environment.  So be careful
 *	out there!  One option is to allocate data structures as 
 * 	automatics within a procedure, but if you do this, you have to
 *	be careful to allocate a big enough stack to hold the automatics!
 */

#include "syscall.h"

int
main ()
{

void autre(void * arg)
{
   int i =1;
   i = i+1;
   
   PutChar('Z');
   UserThreadExit();


}

void inutile(void * arg)
{
   int i = 65 ;
   i = i+1;
   PutChar(i);
   PutChar('Z');

   char *m="JKLM";
   PutString(m);
   i = i+1;
 
//   Exit(1);
}


   int z = 174;
   int *ptr2 = &z;
//   UserThreadCreate(inutile,  (void *)ptr2); 

   UserThreadCreate(autre,  (void *)ptr2); 
   UserThreadCreate(autre,  (void *)ptr2); 

PutChar('E');
PutChar('F');
  
int   a=3;
a=a+1;

/* 
  int cp = 0;
   for(cp = 0; cp<100; cp++){
     a = a+cp-cp;
   }  
*/

 //  Halt ();
//Exit(1);



    /* not reached */
   return 0;
}
